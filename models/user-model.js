const { Sequelize, DataTypes, hash } = require('sequelize');
const sequelize = require('../config/database');
const bcrypt = require('bcryptjs');
const moment = require('moment');

const User = sequelize.define('user', {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true,
    },
    fullname: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    email: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true
    },
    password: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    phone: {
        type: Sequelize.STRING,
    },
    birthday: {
        type: Sequelize.DATEONLY,
    },
    dob: {
        type: DataTypes.VIRTUAL,
        get() {
            return moment(this.birthday).format('DD MMM YYYY');
        },
    },
    address: {
        type: Sequelize.TEXT,
    }
}, {
    hooks: {
        beforeBulkCreate: (users) => {
            users.forEach(async (user) => {
                const salt = await bcrypt.genSaltSync(10, 'a');
                user.dataValues.password = bcrypt.hashSync(user.dataValues.password, salt);
            });
        },
    },
    instanceMethods: {
        validPassword: (password) => {
            return bcrypt.compareSync(password, this.password);
        }
    }
});

User.prototype.validPassword = async (password, hash) => {
    return await bcrypt.compareSync(password, hash);
}

module.exports = User;

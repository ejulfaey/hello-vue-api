const { faker } = require('@faker-js/faker');

const users = [];

Array.from({ length: 25 }).forEach(() => {

    let bd = faker.date.birthdate({ min: 18, max: 65, mode: 'age' });

    users.push({
        fullname: faker.name.fullName(),
        email: faker.internet.email(),
        password: faker.internet.password(),
        phone: faker.phone.number(),
        birthday: bd.getFullYear() + '-' + ('0' + (bd.getMonth()+1)).slice(-2) + '-' + ('0' + bd.getDate()).slice(-2),
        address: faker.address.streetAddress(),
    });
});
module.exports = users;
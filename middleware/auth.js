const jwt = require("jsonwebtoken");

const verifyToken = (req, res, next) => {
    const token = req.body.token || req.query.token || req.headers['authorization'].split(" ")[1];
    console.log(token);

    if (!token) {
        return res.status(403).send({ error: 'A token is required for authentication' });
    }
    try {
        const decoded = jwt.verify(token, "SRORRbMX2mOG7LiUGtLHdLi4mMrX2VFl1MlcipGzddIrGI2izGOLBVxxN3LH");
        req.user = decoded;
    } catch (err) {
        return res.status(401).send({ error: 'Invalid Token' });
    }
    return next();
};

module.exports = verifyToken;
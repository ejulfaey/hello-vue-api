const express = require('express');
const router = express.Router();
const verifyToken = require('../middleware/auth');

const authController = require('../controllers/auth-controller');
const userController = require('../controllers/user-controller');

router.get('/test', (req, res, next) => {
    res.send({
        msg: 'testing api...'
    });
});

router.post('/register', authController.register);
router.post('/login', authController.login);

router.get('/users', verifyToken, userController.getUsers);

module.exports = router;
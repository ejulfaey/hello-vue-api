require('dotenv').config({ path: __dirname + '/.env' })

const http = require('http');
const express = require('express');
const sequelize = require('./config/database');
const routes = require('./config/routes');

const app = express();
const User = require('./models/user-model');
const userFaker = require('./faker/user-faker');

app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    res.setHeader('Content-type', 'application/json');
    next();
})
app.use(express.urlencoded({ extended: true }));
app.use('/api', routes);

// For seeding user data
// sequelize.sync({ force: true }).then(result => {
// User.bulkCreate(userFaker);
// }).catch(err => console.error(err));

sequelize.sync().then(result => {
}).catch(err => console.error(err));

// for local env
// const server = http.createServer(app);
// server.listen(3000, '127.0.0.1', () => {
//     console.log('started server using 3000...')
// });

// for prod env
module.exports = app;


const User = require('../models/user-model');
const jwt = require('jsonwebtoken');

exports.getUsers = async (req, res) => {

    const users = await User.findAll({
        attributes: ['fullname', 'email', 'phone', 'dob', 'address'],
    });
    res.status(200).send(users);
};
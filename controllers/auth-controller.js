const User = require('../models/user-model');
const { Validator } = require('node-input-validator');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const moment = require('moment');

exports.register = async (req, res) => {

    const v = new Validator(req.body, {
        fullname: 'required|maxLength:50',
        email: 'required|email',
        password: 'required|minLength:8',
        phone: 'required',
        birthday: 'required',
        address: 'required',
    }, {
        'fullname.required': 'fullname is required',
        'email.required': 'email is required',
        'email.email': 'email is invalid',
        'password.required': 'password is required',
        'phone.required': 'phone is required',
        'birthday.required': 'birthday is required',
        'address.required': 'address is required',
    });

    const matched = await v.check();
    let errors = {};
    for (key in v.errors) errors[key] = v.errors[key].message;

    if (!matched) {
        return res.status(400).send({
            errors: errors,
        });
    }

    const emailExists = await User.findOne({ where: { email: req.body.email } });

    if (emailExists) {
        return res.status(400).send({
            errors: {
                email: 'Email address has been used',
            },
        });
    }

    const salt = await bcrypt.genSalt(10);

    const createUser = await User.create({
        fullname: req.body.fullname,
        email: req.body.email,
        password: await bcrypt.hash(req.body.password, salt),
        phone: req.body.phone,
        birthday: req.body.birthday,
        address: req.body.address,
    });

    return res.status(201).json({
        msg: 'Congratulation, your account has been created',
        user: {
            id: createUser.id,
            fullname: createUser.fullname,
            email: createUser.email,
            phone: createUser.phone,
            birthday: createUser.birthday,
            address: createUser.address
        }
    });
};

exports.login = async (req, res) => {
    const v = new Validator(req.body, {
        email: 'required',
        password: 'required',
    }, {
        'email.required': 'email is required',
        'password.required': 'password is required',
    });

    const matched = await v.check();

    if (!matched) {
        let errors = {};
        for (key in v.errors) errors[key] = v.errors[key].message;
        return res.status(400).send({
            errors: errors,
        });
    }

    let user = await User.findOne({ where: { email: req.body.email } });
    if (user) {
        const password_valid = await bcrypt.compare(req.body.password, user.password);
        if (password_valid) {
            const token = jwt.sign({ "id": user.id, "email": user.email, "fullname": user.fullname }, "SRORRbMX2mOG7LiUGtLHdLi4mMrX2VFl1MlcipGzddIrGI2izGOLBVxxN3LH");
            return res.status(200).json({
                msg: `Welcome back, ${user.fullname}`,
                user: {
                    fullname: user.fullname,
                    email: user.email,
                    phone: user.phone,
                    birthday: moment(user.birthday).format('DD MMM YYYY'),
                    address: user.address,
                },
                token: token,
            });
        } else {
            return res.status(400).json({
                errors: {
                    account: 'Incorrect email address or password',
                }
            });
        }
    } else {
        return res.status(404).json({
            errors: {
                account: 'Account does not exists',
            }
        });
    }
};